<?php

$users = [
    [
        'name'  => 'Nikita',
        'group' => 'Студент',
        'email' => 'nikita@mail.ru',
        'phone' => '380-11-11-111'
    ],
    [
        'name'  => 'Alex',
        'group' => 'Преподаватель',
        'email' => 'alex@gmai.com',
        'phone' => '380-22-22-222'
    ],
    [
        'name'  => 'Vova',
        'group' => 'Администратор',
        'email' => 'vova@mail.ru',
        'phone' => '380-33-33-333'
    ],
    [
        'name'  => 'Vlad',
        'group' => 'Преподаватель',
        'email' => 'vlad@mail.ru',
        'phone' => '380-44-44-444'
    ],
    [
        'name'  => 'Leonid',
        'group' => 'Студент',
        'email' => 'leonid@mail.ru',
        'phone' => '380-55-55-555'
    ],
    [
        'name'  => 'Miron',
        'group' => 'Администратор',
        'email' => 'miron@mail.ru',
        'phone' => '380-66-66-666'
    ],
    [
        'name'  => 'Yulia',
        'group' => 'Студент',
        'email' => 'yulia@mail.ru',
        'phone' => '380-77-77-777'
    ],
    [
        'name'  => 'Petr',
        'group' => 'Администратор',
        'email' => 'petr@mail.ru',
        'phone' => '380-88-88-888'
    ],
    [
        'name'  => 'Diana',
        'group' => 'Студент',
        'email' => 'diana@mail.ru',
        'phone' => '380-99-99-999'
    ],
    [
        'name'  => 'Ruslan',
        'group' => 'Студент',
        'email' => 'ruslan@mail.ru',
        'phone' => '380-00-00-000'
    ],
];

if (isset($_POST['age']) && $_POST['age'] < 18) {
    $flashMessage = 'Извините, Вы слишком молоды';
    $flashClass = 'warning';
} else {
    if ($_POST['group'] === 'Студент') {
        $groupMessage = 'студентов';
    } elseif ($_POST['group'] === 'Админ') {
        $groupMessage = 'администраторов';
    } elseif ($_POST['group'] === 'Преподаватель') {
        $groupMessage = 'преподаваталей';
    }

    $flashMessage = "Поздравляем, Вы успешно зарегистрированы и добавлены в список $groupMessage";
    $flashClass = 'success';

    $users[] = [
        'name'  => $_POST['name'],
        'group' => $_POST['group'],
        'email' => $_POST['email'],
        'phone' => $_POST['phone'],
    ];
};
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Все пользователи</title>
    <meta name="Description" content="Учетные данные всех пользователей">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
<header>
    <div
            class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <nav class="my-2 my-md-0 mr-md-3">
            <a class="p-2 text-dark" href="index.php">Все пользователи</a>
            <a class="p-2 text-dark" href="student.php">Студенты</a>
            <a class="p-2 text-dark" href="admin.php">Администраторы</a>
            <a class="p-2 text-dark" href="lecturer.php">Преподаватели</a>
        </nav>
    </div>
    <h1 class="display-3" style="text-align: center;">Главная</h1>
</header>

<div class="row">
    <?php if ($_SERVER['REQUEST_METHOD'] === 'POST'): ?>
        <div class="alert alert-<?= $flashClass ?> " role="alert">
            <h4 class="alert-heading text-center"><?= $flashMessage ?></h4>
        </div>
    <?php endif; ?>

    <div class="form-responsive col-md-5 mx-auto my-5">
        <form action="/" method="post">
            <div class="form-group my-2">
                <label for="name">Имя пользователя</label>
                <input type="text" class="form-control" name="name" placeholder="Введите имя пользователя" required>
            </div>
            <div class="form-group my-2">
                <label for="age">Возраст</label>
                <input type="number" class="form-control" name="age" placeholder="Введите возраст" required>
            </div>
            <div class="form-group my-2">
                <label for="group">Группа</label>
                <select class="form-control" name="group">
                    <option>Студент</option>
                    <option>Админ</option>
                    <option>Преподаватель</option>
                </select>
            </div>
            <div class="form-group my-2">
                <label for="email">Почта</label>
                <input type="email" class="form-control" name="email" placeholder="Введите почту" required>
            </div>
            <div class="form-group my-2">
                <label for="phone">Номер телефона</label>
                <input type="number" class="form-control" name="phone" placeholder="Введите номер телефона" required>
            </div>
            <button type="submit" class="btn btn-primary my-2">Зарегистрировать</button>
        </form>
    </div>

    <div class="table-responsive col-md-5 mx-auto my-5">
        <h3 class='sub-header text-center'>Учетные данные всех пользователей</h3>
        <table class="my-1 table table-bordered table-striped">
            <thead class="text-center">
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Группа</th>
                <th scope="col">Почта</th>
                <th scope="col">Телефон</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <?php foreach ($user as $value): ?>
                        <td>  <?= $value ?> </td>
                    <?php endforeach ?>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>
    </div>


</div>


<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
        crossorigin="anonymous"></script>
</body>

</html>