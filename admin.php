<?php

$users = [
    [
        'name'  => 'Nikita',
        'group' => 'student',
        'email' => 'nikita@mail.ru',
        'phone' => '380-11-11-111'
    ],
    [
        'name'  => 'Alex',
        'group' => 'lecturer',
        'email' => 'alex@gmai.com',
        'phone' => '380-22-22-222'
    ],
    [
        'name'  => 'Vova',
        'group' => 'admin',
        'email' => 'vova@mail.ru',
        'phone' => '380-33-33-333'
    ],
    [
        'name'  => 'Vlad',
        'group' => 'lecturer',
        'email' => 'vlad@mail.ru',
        'phone' => '380-44-44-444'
    ],
    [
        'name'  => 'Leonid',
        'group' => 'student',
        'email' => 'leonid@mail.ru',
        'phone' => '380-55-55-555'
    ],
    [
        'name'  => 'Miron',
        'group' => 'admin',
        'email' => 'miron@mail.ru',
        'phone' => '380-66-66-666'
    ],
    [
        'name'  => 'Yulia',
        'group' => 'student',
        'email' => 'yulia@mail.ru',
        'phone' => '380-77-77-777'
    ],
    [
        'name'  => 'Petr',
        'group' => 'admin',
        'email' => 'petr@mail.ru',
        'phone' => '380-88-88-888'
    ],
    [
        'name'  => 'Diana',
        'group' => 'student',
        'email' => 'diana@mail.ru',
        'phone' => '380-99-99-999'
    ],
    [
        'name'  => 'Ruslan',
        'group' => 'student',
        'email' => 'ruslan@mail.ru',
        'phone' => '380-00-00-000'
    ],
];
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Администраторы</title>
    <meta name="Description" content="Учетные данные администраторов">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
<header>
    <div
            class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <nav class="my-2 my-md-0 mr-md-3">
            <a class="p-2 text-dark" href="index.php">Все пользователи</a>
            <a class="p-2 text-dark" href="student.php">Студенты</a>
            <a class="p-2 text-dark" href="admin.php">Администраторы</a>
            <a class="p-2 text-dark" href="lecturer.php">Преподаватели</a>
        </nav>
    </div>
    <h1 class="display-3" style="text-align: center;">Администраторы</h1>
</header>

<div class="row">
    <div class="table-responsive col-md-5 mx-auto my-5">
        <h3 class='sub-header text-center'>Учетные данные администраторов</h3>
        <table class="my-1 table table-bordered table-striped">
            <thead class="text-center">
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Группа</th>
                <th scope="col">Почта</th>
                <th scope="col">Телефон</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($users as $user): ?>
                <?php if ($user['group'] === 'admin'): ?>
                    <tr>
                        <?php foreach ($user as $value): ?>
                            <td>  <?= $value ?> </td>
                        <?php endforeach ?>
                    </tr>
                <?php endif; ?>
            <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
        crossorigin="anonymous"></script>
</body>

</html>